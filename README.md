# traft-progressor #

progressor is a simple progressbar to be used with a HTML5 2d-canvas.

### Usage ###
Make shure progressor is loaded in head.

    <head> <script src='progressor.min.js'></script> </head>


To get progressor to work you first had to initialize it.

    window.traft.progressor.init(
        'mycanvas'  // the id of your canvas
        50          // height of progressbar
        10          // number of resources you wait to finished loading
        '#ac4711'   // optional border color
        '#de0815'   // optional bar color
    )


progressor updates the bar by itself after it was told that a resource has finished loading. You may want to handle that by adding eventlisteners to your objects.

_Image_

    const i = new Image()
    i.onload = () => { window.traft.progressor.oneLoaded(`🖼 image loaded`) }
    i.src = 'somepic.png'

    // if optional parameter was given progressor will log your message to the console

_Audiofile_

    const a = new Audio()
    a.addEventListener('canplaythrough', () => {
      window.traft.progressor.oneLoaded(`🔊 audio loaded`)
    }, false)
    a.src = 'mycoolsfx.ogg'


To check if all resources were loaded call the finished-function.

    if (window.traft.progressor.finished()) {
        // do something
    }


You also may trigger it's draw-function manually.

    while(!window.traft.progressor.finished()) {
        ... do some naughty stuff with your canvas ...
        window.traft.progressor.draw()
    }