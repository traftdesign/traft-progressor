"use strict";

/* eslint-env browser */
window.traft = window.traft || {};

window.traft.progressor = function () {
  var version = '0.0.2';
  var ctx = null;
  var canW, centerY, barH, max;
  var cur = 0;
  var done = false;
  var barMargin = 1;
  var borderMarginX = 50;
  var borderColor = '#000000';
  var barColor = '#000088';

  var draw = function draw() {
    var borderLength = canW - borderMarginX * 2;
    var borderY = centerY - barH / 2;
    ctx.strokeStyle = borderColor;
    ctx.strokeRect(borderMarginX, borderY, borderLength, barH);
    ctx.fillStyle = barColor;
    ctx.fillRect(borderMarginX + barMargin, borderY + barMargin, borderLength * (cur / max) | 0, barH - barMargin * 2);
  };

  var init = function init(canid, barHeight, size, cborder, cbar) {
    if (!ctx && canid) {
      var can = document.getElementById(canid);
      ctx = can.getContext('2d');
      canW = can.width;
      centerY = canW / 2 | 0;
      barH = barHeight;
      max = size;
      borderColor = cborder || borderColor;
      barColor = cbar || barColor;
    }
  };

  var oneLoaded = function oneLoaded(logmsg) {
    cur++;
    done = cur === max;

    if (logmsg) {
      console.log(logmsg);
    }

    draw();
  };

  var finished = function finished() {
    return done;
  };

  return {
    draw: draw,
    init: init,
    oneLoaded: oneLoaded,
    finished: finished,
    version: version
  };
}();
