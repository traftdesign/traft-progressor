/* eslint-env browser */
window.traft = window.traft || {}
window.traft.progressor = (function () {
  const version = '0.0.2'
  let ctx = null
  let canW, centerY, barH, max
  let cur = 0
  let done = false
  const barMargin = 1
  const borderMarginX = 50
  let borderColor = '#000000'
  let barColor = '#000088'

  const draw = () => {
    const borderLength = canW - borderMarginX * 2
    const borderY = centerY - barH / 2
    ctx.strokeStyle = borderColor
    ctx.strokeRect(borderMarginX, borderY, borderLength, barH)
    ctx.fillStyle = barColor
    ctx.fillRect(borderMarginX + barMargin, borderY + barMargin, borderLength * (cur / max) | 0, barH - barMargin * 2)
  }

  const init = (canid, barHeight, size, cborder, cbar) => {
    if (!ctx && canid) {
      const can = document.getElementById(canid)
      ctx = can.getContext('2d')
      canW = can.width
      centerY = canW / 2 | 0
      barH = barHeight
      max = size
      borderColor = cborder || borderColor
      barColor = cbar || barColor
    }
  }

  const oneLoaded = (logmsg) => {
    cur++
    done = cur === max
    if (logmsg) { console.log(logmsg) }
    draw()
  }

  const finished = () => {
    return done
  }

  return { draw, init, oneLoaded, finished, version }
})()
